<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/[{name}]', function (Request $request, Response $response, array $args) use ($container) {
        // Sample log message
        $container->get('logger')->info("Slim-Skeleton '/' route");

        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });

    $app->get('/slim-tdd/siswa', function (Request $request, Response $response, array $args) use ($container) {
        // Render index view
        return $container->get('renderer')->render($response, 'siswa.phtml', $args);
    });

    $app->post('/slim-tdd/siswa', function (Request $request, Response $response, array $args) use ($container) {
        // Render index view
        // return $container->get('renderer')->render($response, 'siswa.phtml', $args);
        
        // return $request;
        $getData = $request->getParsedBody();
        $data = [
            'nis' => $getData['nis'],
            'nama' => $getData['nama'],
        ];

        return $response->withJson($data, 201);
    });
};
