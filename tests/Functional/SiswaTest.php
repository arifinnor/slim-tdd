<?php

namespace Tests\Functional;

class SiswaTest extends BaseTestCase
{
    /**
     * Test that the index route returns a rendered response
     */
    public function testGetSiswaPage()
    {
        $response = $this->runApp('GET', '/slim-tdd/siswa');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('Siswa', (string) $response->getBody());
    }

    public function testCreateSiswa()
    {
        $data = [
            'nis' => '1234',
            'nama' => 'Aaa'

        ];

        $response = $this->runApp('POST', '/slim-tdd/siswa', $data);

        $this->assertEquals(201, $response->getStatusCode());
    }
}
